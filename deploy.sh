#!/bin/bash -eu
git pull
hugo -D
cd public
for i in $(find recipes/ -type d | tail -n +2 | grep -v "recipes/page$"); do
    head -n 12 ./sw.js > newfile.txt
    echo "    '/$i/index.html'", >> newfile.txt
    tail -n +13 sw.js >> newfile.txt
    mv newfile.txt sw.js
done
for i in $(find recipes/ -type d | tail -n +2 | grep -v "recipes/page$"); do
    head -n 5 ./sw.js > newfile.txt
    echo "    '/$i/index.html'", >> newfile.txt
    tail -n +6 sw.js >> newfile.txt
    mv newfile.txt sw.js
done
cd ..
doas rm -rf /usr/share/nginx/librecipe
doas cp -r public /usr/share/nginx/librecipe
