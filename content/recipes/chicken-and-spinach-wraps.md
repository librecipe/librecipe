---
title: "Chicken and Spinach Wraps"
date: "2022-05-10"
description: ""
tags: ["wraps", "chicken", "spinach", "corn"]
---
## Ingredients

For two persons:

* 300 grams chicken breast
* 1 onion
* 1 tablespoon paprika
* 150 grams canned corn
* 300 grams spinach
* 3 tablespoons crème fraîche
* 50 grams grated cheese
* salt and pepper
* 4 to 6 wraps

## Method of preparation

Cut the onion and chicken in small pieces. Heat up some oil or butter in a pan and fry the chicken with the spices. Later, add the onion and fry for 2 minutes.

Add the spinach. When the spinach has shrunk, add the corn and crème fraîche. Stir everything and add the grated cheese. When the cheese is melted, turn off the stove. Finally, fill the wraps with the spinach-chicken stuffing and enjoy!
