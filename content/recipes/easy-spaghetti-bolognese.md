---
title: "Easy spaghetti bolognese"
date: "2021-03-08"
description: ""
tags: ["spaghetti", "bolognese"]
---
## Ingredients

For four persons:

* 2 cloves of garlic
* 2 onions
* 2 leeks
* 500 grams carrots
* 4 stems ribbed celery
* 300 grams half pork and half beef minced meat
* 500 grams spaghetti
* 800 grams diced tomatoes
* A bouillon cube

## Method of preparation

Cut the garlic and finely chop the onions. Wash the leek and cut it into small rings. Peel the carrots and cut them into small blocks. Cut the celery in bows. Heat up some oil in a frying pan and bake the garlic, onion, leek, carrots and celery for 3 minutes. Add the minced meat, and crumble the bouillon cube and bake for five minutes.

In the meantime, cook the spaghetti according to the instructions on the packaging. Add the diced tomatoes to the vegetable-meat mix and cook the sauce for 10 minutes on a slow fire. Season to taste with pepper. Divide the spaghetti and sauce among the plates.
