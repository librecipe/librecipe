---
title: "Stuffed Zucchini"
date: "2021-03-09"
description: "Vegetarian stuffed zucchini - Easy and delicious"
tags: ["zucchini", "vegetarian", "stuffed", "filled"]
---

## Ingredients

For two people:

* 2 regularly sized zucchinis
* 4 mushrooms
* 1 clove of garlic
* 1 small onion
* 100 grams soft cheese
* Herbs
* Pepper & salt
* Feta cheese (optional)

## Method of preparation

Cut the zucchinis in half and hollow them out such that the peel has a strenght
of approximately 5mm. Keep the pulp in an extra bowl. Now, cut the onion and the
mushroom in small cubes. The garlic should be cut in very small pieces. Add all
of it to the bowl and mix it with the soft cheese. Don't forget to add the
herbs, pepper and salt.

Now, you should fill the hollow zucchini with this mixture. You should bake the
zucchini for approximately 30 minutes in the oven.

In the meantime you could focus on the side dishes. Personally, I like to eat it
with mashed potatoes or with some rice.

Enjoy!
