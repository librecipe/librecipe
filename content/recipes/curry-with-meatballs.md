---
title: "Curry with Meatballs"
date: "2022-09-22"
description: ""
tags: ["meatballs", "rice", "curry"]
---
## Ingredients

For two persons:

* Red curry
* 300gr minced meat
* 1 egg
* Rice
* Bread-crumbs
* Kofta spices
* Wok vegetables, and/or tomatoes

## Method of preparation

Put the minced meat, bread-crumbs, kofta spices and the egg in a bowl. Make sure that the proportions of these ingredients are okay. Mix the all contents of the bowl and make small meatballs.

Stir-fry the meatballs. Cook the rice, cut the tomatoes and/or wok vegetables. As soon as the meatballs are done, add the vegetables, and rice. Finally, add the red curry and heat until done.

Enjoy!
